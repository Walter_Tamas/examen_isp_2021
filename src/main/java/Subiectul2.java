import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.FileReader;

import static javax.swing.JOptionPane.showMessageDialog;

public class Subiectul2 extends JFrame {
    JTextField path;
    JTextArea text;
    JButton execute;


    Subiectul2() {
        this.setTitle("Subiectul 2");
        this.setSize(400, 350);
        setDefaultCloseOperation(this.EXIT_ON_CLOSE);
        init();
        setVisible(true);
    }

    public void init() {
        this.setLayout(null);
        path = new JTextField();
        path.setBounds(20, 20, 300, 20);
        text = new JTextArea();
        text.setBounds(20, 60, 300, 100);
        execute = new JButton("Write");
        execute.setBounds(110, 200, 100, 20);

        execute.addActionListener(new Button());

        add(path);
        add(text);
        add(execute);

    }

    class Button implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {

            try {
                text.setText("");
                BufferedReader in = new BufferedReader(new FileReader(path.getText()));
                String s = new String();
                while ((s = in.readLine()) != null) {
                    text.append(s);
                    text.append("\n");
                }
                in.close();

            } catch (Exception err) {
                System.out.println(err.getMessage());
                showMessageDialog(null, "System cannot find the file specified", "Unknown file", 1);
            }


        }
    }

    public static void main(String[] args) {
        new Subiectul2();
    }
}