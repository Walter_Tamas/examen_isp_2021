
public class Subiectul1 {

    interface I {//interfata
        public void i();//metoda abstracta

    }

    class L implements I {//implementare
        private int a;
        M m;//asociere

        public void b() {

        }

        public void i() {

        }

    }

    class X {

        public void met(L l) {//dependenta
            //do something with l
        }

    }

    class M {
        A a;//agregare
        B b;//compozitie

        M(A a) {
            this.a = a;
            b = new B();
        }
    }

    class A {

    }

    class B {

    }

}